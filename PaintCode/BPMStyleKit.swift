//
//  BPMStyleKit.swift
//  BPM
//
//  Created by Liam Dunne on 08/12/2015.
//  Copyright (c) 2015 Lmd64. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//



import UIKit

public class BPMStyleKit : NSObject {

    //// Cache

    private struct Cache {
        static let customLightBlueColor: UIColor = UIColor(red: 0.702, green: 0.840, blue: 1.000, alpha: 1.000)
        static let customWhiteColor: UIColor = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
        static let customWhiteColorWithAlpha: UIColor = BPMStyleKit.customWhiteColor.colorWithAlpha(0.67)
        static let customBlueColor: UIColor = BPMStyleKit.customLightBlueColor.colorWithBrightness(0.4)
    }

    //// Colors

    public class var customLightBlueColor: UIColor { return Cache.customLightBlueColor }
    public class var customWhiteColor: UIColor { return Cache.customWhiteColor }
    public class var customWhiteColorWithAlpha: UIColor { return Cache.customWhiteColorWithAlpha }
    public class var customBlueColor: UIColor { return Cache.customBlueColor }

    //// Drawing Methods

    public class func drawLogo(frame frame: CGRect = CGRectMake(0, 0, 1024, 1024)) {


        //// Subframes
        let group2: CGRect = CGRectMake(frame.minX, frame.minY, frame.width, frame.height)


        //// Group 2
        //// Rectangle Drawing
        let rectanglePath = UIBezierPath(rect: CGRectMake(group2.minX + floor(group2.width * 0.00000 + 0.5), group2.minY + floor(group2.height * 0.00000 + 0.5), floor(group2.width * 1.00000 + 0.5) - floor(group2.width * 0.00000 + 0.5), floor(group2.height * 1.00000 + 0.5) - floor(group2.height * 0.00000 + 0.5)))
        BPMStyleKit.customLightBlueColor.setFill()
        rectanglePath.fill()


        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.12109 + 0.5), group2.minY + floor(group2.height * 0.40723 + 0.5), floor(group2.width * 0.30664 + 0.5) - floor(group2.width * 0.12109 + 0.5), floor(group2.height * 0.59375 + 0.5) - floor(group2.height * 0.40723 + 0.5)))
        BPMStyleKit.customWhiteColor.setFill()
        ovalPath.fill()


        //// Oval 2 Drawing
        let oval2Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.36328 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.48730 + 0.5) - floor(group2.width * 0.36328 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval2Path.fill()


        //// Oval 3 Drawing
        let oval3Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.54199 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.66602 + 0.5) - floor(group2.width * 0.54199 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval3Path.fill()


        //// Oval 4 Drawing
        let oval4Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.72363 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.84766 + 0.5) - floor(group2.width * 0.72363 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval4Path.fill()
    }

    public class func drawRoundLogo(frame frame: CGRect = CGRectMake(0, 0, 1024, 1024)) {


        //// Subframes
        let group2: CGRect = CGRectMake(frame.minX, frame.minY, frame.width, frame.height)


        //// Group 2
        //// Rectangle Drawing
        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(group2.minX + floor(group2.width * 0.00000 + 0.5), group2.minY + floor(group2.height * 0.00000 + 0.5), floor(group2.width * 1.00000 + 0.5) - floor(group2.width * 0.00000 + 0.5), floor(group2.height * 1.00000 + 0.5) - floor(group2.height * 0.00000 + 0.5)), cornerRadius: 512)
        BPMStyleKit.customLightBlueColor.setFill()
        rectanglePath.fill()


        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.12109 + 0.5), group2.minY + floor(group2.height * 0.40723 + 0.5), floor(group2.width * 0.30664 + 0.5) - floor(group2.width * 0.12109 + 0.5), floor(group2.height * 0.59375 + 0.5) - floor(group2.height * 0.40723 + 0.5)))
        BPMStyleKit.customWhiteColor.setFill()
        ovalPath.fill()


        //// Oval 2 Drawing
        let oval2Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.36328 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.48730 + 0.5) - floor(group2.width * 0.36328 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval2Path.fill()


        //// Oval 3 Drawing
        let oval3Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.54199 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.66602 + 0.5) - floor(group2.width * 0.54199 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval3Path.fill()


        //// Oval 4 Drawing
        let oval4Path = UIBezierPath(ovalInRect: CGRectMake(group2.minX + floor(group2.width * 0.72363 + 0.5), group2.minY + floor(group2.height * 0.43848 + 0.5), floor(group2.width * 0.84766 + 0.5) - floor(group2.width * 0.72363 + 0.5), floor(group2.height * 0.56250 + 0.5) - floor(group2.height * 0.43848 + 0.5)))
        BPMStyleKit.customWhiteColorWithAlpha.setFill()
        oval4Path.fill()
    }

}



extension UIColor {
    func colorWithHue(newHue: CGFloat) -> UIColor {
        var saturation: CGFloat = 1.0, brightness: CGFloat = 1.0, alpha: CGFloat = 1.0
        self.getHue(nil, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return UIColor(hue: newHue, saturation: saturation, brightness: brightness, alpha: alpha)
    }
    func colorWithSaturation(newSaturation: CGFloat) -> UIColor {
        var hue: CGFloat = 1.0, brightness: CGFloat = 1.0, alpha: CGFloat = 1.0
        self.getHue(&hue, saturation: nil, brightness: &brightness, alpha: &alpha)
        return UIColor(hue: hue, saturation: newSaturation, brightness: brightness, alpha: alpha)
    }
    func colorWithBrightness(newBrightness: CGFloat) -> UIColor {
        var hue: CGFloat = 1.0, saturation: CGFloat = 1.0, alpha: CGFloat = 1.0
        self.getHue(&hue, saturation: &saturation, brightness: nil, alpha: &alpha)
        return UIColor(hue: hue, saturation: saturation, brightness: newBrightness, alpha: alpha)
    }
    func colorWithAlpha(newAlpha: CGFloat) -> UIColor {
        var hue: CGFloat = 1.0, saturation: CGFloat = 1.0, brightness: CGFloat = 1.0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil)
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: newAlpha)
    }
    func colorWithHighlight(highlight: CGFloat) -> UIColor {
        var red: CGFloat = 1.0, green: CGFloat = 1.0, blue: CGFloat = 1.0, alpha: CGFloat = 1.0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: red * (1-highlight) + highlight, green: green * (1-highlight) + highlight, blue: blue * (1-highlight) + highlight, alpha: alpha * (1-highlight) + highlight)
    }
    func colorWithShadow(shadow: CGFloat) -> UIColor {
        var red: CGFloat = 1.0, green: CGFloat = 1.0, blue: CGFloat = 1.0, alpha: CGFloat = 1.0
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return UIColor(red: red * (1-shadow), green: green * (1-shadow), blue: blue * (1-shadow), alpha: alpha * (1-shadow) + shadow)
    }
}
