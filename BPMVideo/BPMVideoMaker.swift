//
//  BPMVideoMaker.swift
//  
//
//  Created by Liam Dunne on 09/12/2015.
//
//

import UIKit
import AVKit
import AVFoundation

public class BPMVideoMaker: NSObject {
    
    let operationQueue = NSOperationQueue()
    var renderOperation :NSOperation?

    override public init() {
        operationQueue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount
        operationQueue.qualityOfService = .Background
    }
    
    public func playerViewControllerForClip(clip :VideoClip) -> (AVPlayerViewController?) {
        if let urlString = clip.file?.url as String? {
            if let url = NSURL(string: urlString) {
                let playerViewController = AVPlayerViewController()
                playerViewController.player = AVPlayer(URL: url)
                
                playerViewController.videoGravity = AVLayerVideoGravityResizeAspectFill
                playerViewController.showsPlaybackControls = false
                
                playerViewController.view.addShadow()
                
                return playerViewController
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    public func previewVideo(video :Video) -> (NSOperation) {
        let operation = NSOperation()
        return operation
    }
    
    public func renderFinalVideo(video :Video) -> (NSOperation) {
        let operation = NSOperation()
        return operation
    }

    public func cancelOperation(operation :NSOperation){
        operation.cancel()
    }

    public func renderClipsForVideo(video :Video) -> (NSOperation) {
        let operation = NSOperation()
        return operation
    }
    
}
