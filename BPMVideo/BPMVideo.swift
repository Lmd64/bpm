//
//  BPMVideo.swift
//  BPM
//
//  Created by Liam Dunne on 15/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import AVFoundation

public class BPMVideoFile: NSObject, NSCoding {
    
    public var title: String
    public var image: UIImage?
    public var url: NSURL?

    override init() {
        self.title = " "
        super.init()
    }
    
    public func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(title, forKey: "title")
    }
    required public init?(coder aDecoder: NSCoder){
        title = aDecoder.decodeObjectForKey("title") as! String
    }

    public func thumbnailFromURL(url :NSURL) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        var time = asset.duration
        time.value = 0
        do {
            let imageRef = try imageGenerator.copyCGImageAtTime(time, actualTime: nil)
            let thumbnail = UIImage(CGImage: imageRef)
            let rotatedThumbnail = thumbnail.imageRotatedByDegrees(90.0, flip: false)
//            print("thumbnail        = \(thumbnail.size)")
//            print("rotatedThumbnail = \(rotatedThumbnail.size)")
            return rotatedThumbnail

        } catch {
            return nil
        }
    }

    func rotate(image: UIImage) -> UIImage {
        
        var transform = CGAffineTransformIdentity
        transform = CGAffineTransformTranslate(transform, 0, image.size.height)
        transform = CGAffineTransformRotate(transform, -CGFloat(M_PI_2))
        
        switch image.imageOrientation {
        case .UpMirrored, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
            
        case .LeftMirrored, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
        default:
            transform = CGAffineTransformScale(transform, 1, 1)
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGBitmapContextCreate(nil, Int(image.size.height), Int(image.size.width),
            CGImageGetBitsPerComponent(image.CGImage), 0,
            CGImageGetColorSpace(image.CGImage),
            UInt32(CGImageGetBitmapInfo(image.CGImage).rawValue))
        
        CGContextConcatCTM(ctx, transform)
        
        switch image.imageOrientation {
        case .Left, .LeftMirrored, .Right, .RightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = CGBitmapContextCreateImage(ctx)
        let img = UIImage(CGImage: cgimg!)
        return img
    }

}

public class BPMVideoClip: NSObject,NSCoding {
    
    public var hasVideo :Bool
    public var isDownbeat :Bool
    public var file :BPMVideoFile
    var defaultColor :UIColor
    
    override init() {
        self.hasVideo = false
        self.isDownbeat = false
        self.file = BPMVideoFile.init()
        self.defaultColor = UIColor.whiteColor()
        super.init()
    }

    public func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(hasVideo, forKey: "hasVideo")
        aCoder.encodeObject(isDownbeat, forKey: "isDownbeat")
        aCoder.encodeObject(file, forKey: "file")
        aCoder.encodeObject(defaultColor, forKey: "defaultColor")
    }

    required public init?(coder aDecoder: NSCoder){
        hasVideo = aDecoder.decodeObjectForKey("hasVideo") as! Bool
        isDownbeat = aDecoder.decodeObjectForKey("isDownbeat") as! Bool
        file = aDecoder.decodeObjectForKey("file") as! BPMVideoFile
        defaultColor = aDecoder.decodeObjectForKey("defaultColor") as! UIColor
    }

    public func recordVideo(){
        hasVideo = true
    }
    
    public func clipColor() -> UIColor {
        if hasVideo {
            if isDownbeat {
                return defaultColor.colorWithAlphaComponent(1.0)
            } else {
                return defaultColor.colorWithAlphaComponent(0.5)
            }
        } else {
            return defaultColor.colorWithAlphaComponent(0.1)
        }
    }
    
}

public class BPMVideo: NSObject,NSCoding {
    public var title: String
    public var bpm: Float
    public var multiple: Int
    public var clips: Array<BPMVideoClip>
    public var count: Int
    public var dateCreated : NSDate
    public var dateUpdated : NSDate

    override public init() {
        self.title = "<title>"
        self.bpm = 120.0
        self.multiple = 4
        self.clips = []
        self.count = 16
        self.dateCreated = NSDate()
        self.dateUpdated = NSDate()
        super.init()
    }
    
    public func encodeWithCoder(aCoder: NSCoder){
        aCoder.encodeObject(bpm, forKey: "bpm")
        aCoder.encodeObject(multiple, forKey: "multiple")
        aCoder.encodeObject(clips, forKey: "clips")
        aCoder.encodeObject(count, forKey: "count")
        aCoder.encodeObject(dateCreated, forKey: "dateCreated")
        aCoder.encodeObject(dateUpdated, forKey: "dateUpdated")
    }
 
    required public init?(coder aDecoder: NSCoder){
        title = aDecoder.decodeObjectForKey("title") as! String
        bpm = aDecoder.decodeObjectForKey("bpm") as! Float
        multiple = aDecoder.decodeObjectForKey("multiple") as! Int
        clips = aDecoder.decodeObjectForKey("clips") as! Array<BPMVideoClip>
        count = aDecoder.decodeObjectForKey("count") as! Int
        dateCreated = aDecoder.decodeObjectForKey("dateCreated") as! NSDate
        dateUpdated = aDecoder.decodeObjectForKey("dateUpdated") as! NSDate
    }

    func customDescription() -> String {
        var customDescription = ""
        
        customDescription = customDescription.stringByAppendingString("count: \(count), ")
        customDescription = customDescription.stringByAppendingString("clipDuration: \(clipDuration()),")
        
        for (_, clip) in clips.enumerate() {
            let title = clip.file.title
            if clip.isDownbeat {
                customDescription = customDescription.stringByAppendingString("\n[\(title)],")
            } else {
                customDescription = customDescription.stringByAppendingString(" \(title),")
            }
        }
        return customDescription
    }
    
    public func clipDuration() -> Float {
        let clipDuration = 60.0 * Float(multiple) / bpm
        return clipDuration
    }

    public func totalDuration() -> Float {
        let totalDuration = Float(count) * self.clipDuration()
        return totalDuration
    }
    
    public func generateClips(){
        self.clips = []
        for index in 0...count-1 {
            let clip = BPMVideoClip.init()
            let isDownBeat = (beatForIndex(index) == 0)
            clip.isDownbeat = isDownBeat
            clip.defaultColor = defaultColorForIndex(index)
            clips.append(clip)
        }
    }

    func beatForIndex(index :Int) ->Int {
        return index % multiple
    }
    
    func defaultColorForIndex(index :Int) -> UIColor {
        let beatIndex = beatForIndex(index)
        let hue = CGFloat(Float(beatIndex) / Float(multiple))
        var brightness = CGFloat(
            Float(index / multiple) /
            Float(count / multiple)
        )
        brightness = (1.0 - brightness) * 0.5 + 0.25
        
        let color = UIColor(hue: hue, saturation: 1.0, brightness: brightness, alpha: 1.0)
        return color
    }
    
//    public func recordClipAtIndex(index :Int){
//        let clip = clips[index]
//        clip.recordVideo()
//        clip.file.title = "\(index)"
//    }
    
    public func copyClipToAll(clipToCopy :BPMVideoClip){
        for (_, clip) in clips.enumerate() {
            if !clipToCopy.isEqual(clip) {
                clip.file = clipToCopy.file
                clip.defaultColor = clipToCopy.defaultColor
                clip.hasVideo = clipToCopy.hasVideo
            }
        }
    }
    
    public func copyClipToAllOnSameBeat(clipToCopy :BPMVideoClip){
        let indexOfClipToCopy = clips.indexOf(clipToCopy)!
        let beatOfClipToCopy = beatForIndex(indexOfClipToCopy)
        for (index, clip) in clips.enumerate() {
            let beatOfClip = beatForIndex(index)
            if !clipToCopy.isEqual(clip) && beatOfClipToCopy==beatOfClip {
                clip.file = clipToCopy.file
                clip.defaultColor = clipToCopy.defaultColor
                clip.hasVideo = clipToCopy.hasVideo
            }
        }
    }
 
    public func clipAtIndexPath(indexPath :NSIndexPath) -> BPMVideoClip {
        let section = indexPath.section
        let row = indexPath.row
        let index = section * multiple + row
        return clips[index]
    }
    
}