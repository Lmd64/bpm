//
//  BPMVideo.h
//  BPMVideo
//
//  Created by Liam Dunne on 15/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BPMVideo.
FOUNDATION_EXPORT double BPMVideoVersionNumber;

//! Project version string for BPMVideo.
FOUNDATION_EXPORT const unsigned char BPMVideoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BPMVideo/PublicHeader.h>

