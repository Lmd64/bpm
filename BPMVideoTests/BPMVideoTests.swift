//
//  BPMVideoTests.swift
//  BPMVideoTests
//
//  Created by Liam Dunne on 15/10/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import XCTest
@testable import BPMVideo

class BPMVideoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateDefaultVideo() {

        let video = BPMVideo.init()
        video.generateClips()
        let countOfClips = video.countOfClips()
        XCTAssert(countOfClips==16,"countOfClips==\(countOfClips)")

        print("\(video.customDescription)")

    }
    
    func testCreateCustomVideo() {
        
        let video = BPMVideo.init()
        video.duration = 2.0*60.0 + 32.0
        video.bpm = 127
        video.multiple = 3
        video.generateClips()
        
        let countOfClips = video.countOfClips()
        XCTAssert(countOfClips==107,"countOfClips==\(countOfClips)")
        
        print("\(video.customDescription)")
        
    }

    func testRecordAndCopyClips() {
        
        let video = BPMVideo.init()
        video.generateClips()

        let countOfClips = video.countOfClips()
        XCTAssert(countOfClips==16,"countOfClips==\(countOfClips)")

        //record into three clips
        video.recordClipAtIndex(0)
        video.recordClipAtIndex(2)
        video.recordClipAtIndex(5)
        video.recordClipAtIndex(7)
        
        //copy clip at index 0 into other downbeats
        video.clips[4].file = video.clips[0].file
        video.clips[8].file = video.clips[0].file
        video.clips[12].file = video.clips[0].file
        
        print("\(video.customDescription())")
        
    }
    
    func testRecordAndCopyToAllClips() {
        
        let video = BPMVideo.init()
        video.generateClips()
        
        let countOfClips = video.countOfClips()
        XCTAssert(countOfClips==16,"countOfClips==\(countOfClips)")
        
        //record into four clips
        video.recordClipAtIndex(0)
        video.recordClipAtIndex(2)
        video.recordClipAtIndex(5)
        video.recordClipAtIndex(7)
        
        //copy clip at index 2 into all clips
        video.copyClipToAll(video.clips[2])
        
        print("\(video.customDescription)")
        
    }
    
    func testRecordAndCopyToAllClipsOnSameBeat() {
        
        let video = BPMVideo.init()
        video.generateClips()
        
        //record into four clips
        video.recordClipAtIndex(0)
        video.recordClipAtIndex(2)
        video.recordClipAtIndex(5)
        video.recordClipAtIndex(7)
        
        //copy clip at index 2 into other downbeats
        video.copyClipToAllOnSameBeat(video.clips[2])
        video.copyClipToAllOnSameBeat(video.clips[5])
        video.copyClipToAllOnSameBeat(video.clips[7])
        video.copyClipToAllOnSameBeat(video.clips[0])
        
        print("\(video.customDescription())")
        
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
