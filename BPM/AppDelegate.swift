//
//  AppDelegate.swift
//  BPM
//
//  Created by Liam Dunne on 08/12/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import AlamofireImage
import LLSimpleCamera
import iOS_Check_Permission
import BPMVideo

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var camera: LLSimpleCamera?
    let coreDataManager = CoreDataManager.sharedInstance

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.

        //nukeData()
                
//        testAlamofire()
//        testCheckPermissions()
//        testLLSimpleCamera()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.sharedInstance.saveContext()
    }

    func testAlamofire(){

        Alamofire.request(.GET, "https://httpbin.org/get", parameters: ["foo": "bar"])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
        }

    }

    func testAlamofireImage(){

        Alamofire.request(.GET, "https://httpbin.org/image/png")
            .responseImage { response in
                debugPrint(response)
                
                print(response.request)
                print(response.response)
                debugPrint(response.result)
                
                if let image = response.result.value {
                    print("image downloaded: \(image.size)")
                }
        }

    }

    func testCheckPermissions(){
        IOSCheckPermissions.globalInstance().checkPermissionAccessForGallery(
            {
                //success
                print("Access granted to library!")
            
            }, failureBlock: {
                //failure
                print("Can't get access to library, check your settings")
                
            }, authorizationStatusNotDetermined: {
                //status not determined
                print("Can't determine auth status")
            }
        )
    }

}
