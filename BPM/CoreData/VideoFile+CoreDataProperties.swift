//
//  VideoFile+CoreDataProperties.swift
//  BPM
//
//  Created by Liam Dunne on 11/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension VideoFile {

    @NSManaged var title: String?
    @NSManaged var imageData: NSData?
    @NSManaged var url: String?
    @NSManaged var clip: NSManagedObject?

}
