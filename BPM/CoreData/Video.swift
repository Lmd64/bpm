//
//  Video.swift
//  BPM
//
//  Created by Liam Dunne on 11/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import UIKit
import CoreData

public class Video: BPMManagedObject {

// Insert code here to add functionality to your managed object subclass

    class public func instanceWithBPMVideo(bpmVideo :BPMVideo) -> Video {
        let context = CoreDataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName(String(self), inManagedObjectContext:context!)
        let video = Video.init(entity: entityDescription!, insertIntoManagedObjectContext:context)
        
        video.title = bpmVideo.title
        video.bpm = bpmVideo.bpm
        video.multiple = bpmVideo.multiple
        video.count = bpmVideo.count
        video.dateCreated = bpmVideo.dateCreated
        video.dateUpdated = bpmVideo.dateUpdated
        
        return video
    }

    func customDescription() -> String {
        var customDescription = ""
        
        customDescription = customDescription.stringByAppendingString("title: \(title), ")
        customDescription = customDescription.stringByAppendingString("count: \(count), ")
        customDescription = customDescription.stringByAppendingString("clipDuration: \(clipDuration()),")
        
        customDescription = customDescription.stringByAppendingString("clips: \n")
        clips?.enumerateObjectsUsingBlock { (elem, idx, stop) -> Void in
            if let clip = elem as? VideoClip {
                customDescription = customDescription.stringByAppendingString("\tclip\(idx): \(clip.customDescription())")
            }
        }
        customDescription = customDescription.stringByAppendingString("\n")

        return customDescription
    }
    
    public func clipDuration() -> Float {
        let clipDuration = 60.0 * Float(multiple ?? 0.0) / Float(bpm ?? 0.0)
        return clipDuration
    }
    
    public func totalDuration() -> Float {
        let totalDuration = Float(count ?? 0.0) * self.clipDuration()
        return totalDuration
    }
    
    public func generateClipsIfNeeded(){

        if let context = self.managedObjectContext {
            
            if clips?.count == 0 || clips?.count != count {

                removeAllClips()

                for index in 0...Int(count ?? 0)-1 {
                    let clip = VideoClip.instanceWithVideo(self)
                    let downBeat = (beatForIndex(index) == 0)
                    clip.downbeat = downBeat
                    clip.defaultColor = defaultColorForIndex(index)
                }
                
                do {
                    try context.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            
            }
        }
        
    }
    
    func barForIndex(index :Int) ->Int {
        return Int(index/Int(multiple ?? 0))
    }
    func beatForIndex(index :Int) ->Int {
        return index % (Int(multiple ?? 0))
    }
    
    func defaultColorForIndex(index :Int) -> UIColor {

        let barIndex = barForIndex(index)
        let beatIndex = beatForIndex(index)
        let mult = Float(multiple ?? 1.0)
        let barCount = Int(count ?? 1)/Int(multiple ?? 1)

//        let range = CGFloat( (Float(index) / mult) / (Float(count ?? 1) / mult) )

        //cycle color per beat
        let hue = CGFloat(Float(beatIndex) / mult)
        
        //don't affect brightness
        let brightness = CGFloat(1.0) //(1.0 - range) * 0.5 + 0.25
        
        //step saturation down per bar
        let saturation = CGFloat(Float(barCount - barIndex) / Float(barCount))

        let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        return color
    }
        
//    public func copyClipToAll(clipToCopy :VideoClip){
//        clips?.enumerateObjectsUsingBlock { (elem, idx, stop) -> Void in
//            if let clip :VideoClip = elem as? VideoClip {
//                if !clipToCopy.isEqual(clip) {
//                    clip.file = clipToCopy.file
//                    clip.defaultColor = clipToCopy.defaultColor
//                }
//            }
//        }
//    }
//    
//    public func copyClipToAllOnSameBeat(clipToCopy :VideoClip){
//        if let indexOfClipToCopy = clips?.indexOfObject(clipToCopy) {
//            let beatOfClipToCopy = beatForIndex(indexOfClipToCopy)
//            clips?.enumerateObjectsUsingBlock { (elem, idx, stop) -> Void in
//                if let clip :VideoClip = elem as? VideoClip {
//                    let beatOfClip = self.beatForIndex(idx)
//                    if !clipToCopy.isEqual(clip) && beatOfClipToCopy==beatOfClip {
//                        clip.file = clipToCopy.file
//                        clip.defaultColor = clipToCopy.defaultColor
//                    }
//                }
//            }
//        }
//    }
    
    public func clipAtIndexPath(indexPath :NSIndexPath) -> VideoClip? {
        let section = indexPath.section
        let row = indexPath.row
        let index = section * Int(multiple ?? 0) + row
        if let clip :VideoClip = clips?.objectAtIndex(index) as? VideoClip {
            return clip
        } else {
            return nil
        }
    }

    public func appendClip(clip :VideoClip){
        if let mutableItems = clips?.mutableCopy() as? NSMutableOrderedSet {
            mutableItems.addObject(clip)
            clips = mutableItems.copy() as? NSOrderedSet
        }
    }
    public func insertClip(clip :VideoClip, toIndexPath :NSIndexPath){
        if let mutableItems = clips?.mutableCopy() as? NSMutableOrderedSet {
            mutableItems.insertObject(clip, atIndex: toIndexPath.row)
            clips = mutableItems.copy() as? NSOrderedSet
        }
    }
    public func removeClip(clip :VideoClip){
        if let mutableItems = clips?.mutableCopy() as? NSMutableOrderedSet {
            mutableItems.removeObject(clip)
            clips = mutableItems.copy() as? NSOrderedSet
        }
    }
    public func removeAllClips(){
        clips?.enumerateObjectsUsingBlock { (elem, idx, stop) -> Void in
            if let clip :VideoClip = elem as? VideoClip {
                self.removeClip(clip)
            }
        }
    }
    
}
