//
//  VideoClip+CoreDataProperties.swift
//  BPM
//
//  Created by Liam Dunne on 14/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension VideoClip {

    @NSManaged var defaultColor: NSObject?
    @NSManaged var downbeat: NSNumber?
    @NSManaged var file: VideoFile?
    @NSManaged var video: Video?

}
