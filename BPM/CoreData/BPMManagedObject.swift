//
//  BPMManagedObject.swift
//  BPM
//
//  Created by Liam Dunne on 11/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit
import CoreData

public class BPMManagedObject: NSManagedObject {
    
    public class func defaultContextForClass() -> NSManagedObjectContext{
        let context = CoreDataManager.sharedInstance.managedObjectContext
        return context!
    }

    public class func allObjects() -> [AnyObject]? {
        let context = self.defaultContextForClass()
        //let fetchRequest = NSFetchRequest(entityName:NSStringFromClass(self.dynamicType))
        let fetchRequest = NSFetchRequest(entityName:String(self))

        // Add Predicate
        //var predicate = nil;
        //fetchRequest.predicate = predicate

        // Add Sort Descriptors
        //let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
        //fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = []
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        //fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
        
        return fetchedResultsController.fetchedObjects
    }

}
