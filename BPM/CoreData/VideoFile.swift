//
//  VideoFile.swift
//  BPM
//
//  Created by Liam Dunne on 11/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AVFoundation

public class VideoFile: BPMManagedObject {

// Insert code here to add functionality to your managed object subclass

    class public func instanceWithClip(clip :VideoClip) -> VideoFile {
        let context = CoreDataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName(String(self), inManagedObjectContext:context!)
        
        let file = VideoFile.init(entity: entityDescription!, insertIntoManagedObjectContext:context)
        file.clip = clip
        
        let filename = NSUUID().UUIDString
        file.url = CoreDataManager.sharedInstance.applicationDocumentsDirectory.URLByAppendingPathComponent(filename).URLByAppendingPathExtension("mov").absoluteString
        
        return file
    }

    func customDescription() -> String {
        var customDescription = ""

        if self.imageData != nil {
            customDescription = customDescription.stringByAppendingString("<ImageData>, ")
        }
        if let url = self.url as String? {
            customDescription = customDescription.stringByAppendingString("url: \(url)")
        }
        
        return customDescription
    }
    
    func setImage(image: UIImage) {
        imageData = UIImageJPEGRepresentation(image, 1.0)
    }
    
    func image() -> UIImage? {
        if let data = imageData {
            return UIImage(data: data)
        } else {
            return nil
        }
    }

    public func thumbnailFromURL(url :NSURL) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        var time = asset.duration
        time.value = 0
        do {
            let imageRef = try imageGenerator.copyCGImageAtTime(time, actualTime: nil)
            let thumbnail = UIImage(CGImage: imageRef)
            let rotatedThumbnail = thumbnail.imageRotatedByDegrees(90.0, flip: false)
            //print("thumbnail        = \(thumbnail.size)")
            //print("rotatedThumbnail = \(rotatedThumbnail.size)")
            return rotatedThumbnail
            
        } catch {
            return nil
        }
    }
    
    func rotate(image: UIImage) -> UIImage {
        
        var transform = CGAffineTransformIdentity
        transform = CGAffineTransformTranslate(transform, 0, image.size.height)
        transform = CGAffineTransformRotate(transform, -CGFloat(M_PI_2))
        
        switch image.imageOrientation {
        case .UpMirrored, .DownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
            
        case .LeftMirrored, .RightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
        default:
            transform = CGAffineTransformScale(transform, 1, 1)
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGBitmapContextCreate(nil, Int(image.size.height), Int(image.size.width),
            CGImageGetBitsPerComponent(image.CGImage), 0,
            CGImageGetColorSpace(image.CGImage),
            UInt32(CGImageGetBitmapInfo(image.CGImage).rawValue))
        
        CGContextConcatCTM(ctx, transform)
        
        switch image.imageOrientation {
        case .Left, .LeftMirrored, .Right, .RightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = CGBitmapContextCreateImage(ctx)
        let img = UIImage(CGImage: cgimg!)
        return img
    }

}
