//
//  Video+CoreDataProperties.swift
//  BPM
//
//  Created by Liam Dunne on 14/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension Video {

    @NSManaged var bpm: NSNumber?
    @NSManaged var count: NSNumber?
    @NSManaged var multiple: NSNumber?
    @NSManaged var title: String?
    @NSManaged var dateCreated: NSDate?
    @NSManaged var dateUpdated: NSDate?
    @NSManaged var clips: NSOrderedSet?

}
