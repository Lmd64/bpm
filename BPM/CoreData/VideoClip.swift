//
//  VideoClip.swift
//  BPM
//
//  Created by Liam Dunne on 11/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import Foundation
import UIKit
import CoreData

public class VideoClip: BPMManagedObject {

// Insert code here to add functionality to your managed object subclass

    class public func instanceWithVideo(video :Video) -> VideoClip {
        let context = CoreDataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName(String(self), inManagedObjectContext:context!)
        
        let clip = VideoClip.init(entity: entityDescription!, insertIntoManagedObjectContext:context)
        clip.video = video
        return clip
    }

    func customDescription() -> String {
        var customDescription = ""

        if self.downbeat == true {
            customDescription = customDescription.stringByAppendingString("Downbeat, ")
        }
        
        if let file = self.file as VideoFile? {
            customDescription = customDescription.stringByAppendingString("\t\tfile: \(file.customDescription()), ")
        }
        customDescription = customDescription.stringByAppendingString("\n")
        
        return customDescription
    }

    public func clipColor() -> UIColor {
        if let color = defaultColor as? UIColor {
            if file != nil {
                if downbeat == true {
                    return color.colorWithAlphaComponent(1.0)
                } else {
                    return color.colorWithAlphaComponent(0.5)
                }
            } else {
                return color.colorWithAlphaComponent(0.1)
            }
        } else {
            return UIColor.grayColor()
        }
    }

    public func fileExists() -> Bool {
        if let urlString = self.file?.url as String? {
            let url = NSURL(string: urlString)
            if let path = url?.path {
                let fileExists = NSFileManager().fileExistsAtPath(path)
                return fileExists
            }
        }
        return false
    }
    
}
