//
//  UIView+Extension.swift
//  BPM
//
//  Created by Liam Dunne on 14/12/2015.
//  Copyright © 2015 Lmd64. All rights reserved.
//

import UIKit

extension UIView {
    
//    func centerHorizontally(parentWidth: Double) {
//        frame = CGRect(x: floor((parentWidth - frame.size.width) / 2.0), y: frame.origin.y, width: frame.size.width, height: frame.size.height)
//    }
    
    func addShadow() {
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSizeMake(0, 4)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 5.0
    }
    
    func addSmallShadow() {
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSizeMake(0, 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2.5
    }
    
}
