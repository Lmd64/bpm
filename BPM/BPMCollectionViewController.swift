//
//  CollectionViewController.swift
//  BPM
//
//  Created by Liam Dunne on 08/12/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

import UIKit
import BPMVideo
import LLSimpleCamera
import AVKit

let reuseIdentifierCell = "BPMCollectionViewCell"
let reuseIdentifierHeader = "BPMCollectionViewHeader"
let reuseIdentifierFooter = "BPMCollectionViewFooter"


class BPMCollectionViewCell :UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func prepareForReuse() {
        self.backgroundColor = UIColor.clearColor()
    }
}

//class BPMCollectionViewFlowLayout: UICollectionViewFlowLayout {
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        self.itemSize = CGSizeMake(80, 80)
//        self.headerReferenceSize = CGSizeMake(0, 0)
//        self.footerReferenceSize = CGSizeMake(0, 0)
//        self.minimumInteritemSpacing = 0
//        self.minimumLineSpacing = 0
//        self.sectionInset = UIEdgeInsetsZero
//    }
//}

public class BPMCollectionViewController: UICollectionViewController, RAReorderableLayoutDelegate, RAReorderableLayoutDataSource, UIGestureRecognizerDelegate {

    public var video :Video!

    var selectedItems: NSArray!
    var camera :LLSimpleCamera!
    var cameraPresenterViewController :UIViewController? = UIViewController()
    var cameraShadowView :UIView? = UIView()
    let coreDataManager = CoreDataManager.sharedInstance
    var cellForClip : BPMCollectionViewCell?
    
    var playerViewController :AVPlayerViewController?
    var player : AVPlayer?
    let bpmVideoMaker = BPMVideoMaker.init()
    var selectedIndexPath :NSIndexPath?
    
    let durationAnimateFrame = 0.44
    let durationAnimateAlpha = 0.22
    

    override public func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        selectedItems = []
        collectionView?.allowsMultipleSelection = true
        
        video.generateClipsIfNeeded()
        
        camera = LLSimpleCamera(quality: AVCaptureSessionPresetHigh, position: CameraPositionFront, videoEnabled: true)
    }

    override public func viewWillAppear(animated: Bool) {
        //camera.start()
        //testAutomation()
        print("\(video.customDescription())")
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func testAutomation(){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            self._add(0)
//        })
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            self._add(2)
//        })
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.6 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            self._add(5)
//        })
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.8 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
//            self._edit()
//        })
    }
    
    func reloadData(){
        self.collectionView?.reloadData()
        print("\(video.customDescription())")
    }
    
    func _add(index :Int){
//        let clip = video.clipAtIndexPath(<#T##indexPath: NSIndexPath##NSIndexPath#>)
//        video.recordClipAtIndex(index)
//        collectionView?.reloadData()
    }
    
    func _edit(){
//        let clip = video.clips[0]
//        video.copyClipToAllOnSameBeat(clip)
//        collectionView?.reloadData()
    }
    
    // MARK: UICollectionViewDataSource

    override public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    override public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(video.count ?? 0)
    }

    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let width:CGFloat = self.view.frame.size.width / CGFloat(video.multiple ?? 1.0)
        return CGSizeMake(width, width)
    }

    override public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:BPMCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifierCell, forIndexPath: indexPath) as! BPMCollectionViewCell

        let clip = video.clipAtIndexPath(indexPath)
        let title = clip?.file?.title
        let clipColor = clip?.clipColor()
        
        // Configure the cell
        cell.titleLabel.text = "\(title)"
        cell.titleLabel.textColor = UIColor.whiteColor()
        
        if clip?.fileExists() == true {
            if let image = clip?.file?.image() as UIImage? {
                cell.imageView.image = image
                cell.imageView.alpha = 1.0
            } else {
                cell.imageView.image = UIImage(imageLiteral: "item")
            }
        } else {
            cell.imageView.image = UIImage(imageLiteral: "item")
//            if clip?.downbeat == true {
//                cell.imageView.alpha = 0.667
//            } else {
//                cell.imageView.alpha = 0.333
//            }
        }

        cell.contentView.backgroundColor = clipColor
        cell.contentView.layer.borderColor = UIColor.clearColor().CGColor
        cell.contentView.layer.borderWidth = 0.0
        
        return cell
    }

    override public func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        if (selectedItems.containsObject(indexPath)){
            return false
        } else {
            return true
        }
    }

    override public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        //var cell = collectionView.cellForItemAtIndexPath(indexPath) as! BPMCollectionViewCell

        if self.playerViewController == nil {
            handleTap(indexPath)
        }
        
//        let section = indexPath.section
//        let row = indexPath.row
//        
//        let complementarySection = ((section % 2==0) ? section+1 : section-1)
//        let complementaryRow = collectionView.numberOfItemsInSection(section) - row - 1
//        let complementaryIndexPath = NSIndexPath(forRow: complementaryRow, inSection: complementarySection)
//
//        for selectedItem:NSIndexPath in selectedItems as! [NSIndexPath]{
//            if ((selectedItem.section == section) && (selectedItem.row != row)) {
//                mutableArray.removeObject(selectedItem)
//            } else if ((selectedItem.section == complementarySection) && (selectedItem.row != complementaryRow)) {
//                mutableArray.removeObject(selectedItem)
//            }
//        }
//        
//        mutableArray.addObject(complementaryIndexPath)
//        
        //self.selectedItems = mutableArray.copy() as! NSArray
        collectionView.reloadData()

    }

    override public func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath){
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! BPMCollectionViewCell
        cell.backgroundColor = UIColor(white:0.9, alpha:0.5)
    }
    
    override public func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    public func collectionView(collectionView: UICollectionView, allowMoveAtIndexPath indexPath: NSIndexPath) -> Bool {
        if collectionView.numberOfItemsInSection(indexPath.section) <= 1 {
            return false
        }
        return true
    }
    
    public func collectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath, didMoveToIndexPath toIndexPath: NSIndexPath) {
        if let atClip = video.clipAtIndexPath(atIndexPath) {
            //        let toClip = video.clipAtIndexPath(toIndexPath)
            //        //video.removeClip(atClip)
            video.insertClip(atClip, toIndexPath: toIndexPath)
        }
    }
    
    //gestures: tap
    func handleTap(indexPath: NSIndexPath) {
        if let clip = video.clipAtIndexPath(indexPath) as VideoClip? {
            if clip.file != nil {
                // we have a video for this clip, play the video
                playClip(clip, indexPath:  indexPath)
                
            } else {
                // we don't have a video for this clip, start recording
                recordVideoForClip(clip, indexPath:  indexPath)
            }
        }
    }

    //record clip
    func endFrameForCameraContainer() ->CGRect {
        let sideLength = min(CGRectGetWidth(self.view.bounds),CGRectGetHeight(self.view.bounds)) - 40.0
        let xOffset = (CGRectGetWidth(self.view.bounds) - sideLength)/2.0
        let yOffset = (CGRectGetHeight(self.view.bounds) - sideLength)/2.0
        
        let endFrame = CGRectMake(xOffset, yOffset, sideLength, sideLength)
        return endFrame
    }

    func endFrameForCamera() ->CGRect {
        let sideLength = min(CGRectGetWidth(self.view.bounds),CGRectGetHeight(self.view.bounds)) - 40.0
        let scale = sideLength / min(CGRectGetWidth(self.view.bounds),CGRectGetHeight(self.view.bounds))
        let width = CGRectGetWidth(self.view.bounds) * scale
        let height = CGRectGetHeight(self.view.bounds) * scale
        
        let xOffset = -(width - sideLength)/2.0
        let yOffset = -(height - sideLength)/2.0
        
        let endFrame = CGRectMake(xOffset, yOffset, width, height)
        return endFrame
    }
    
    func recordVideoForClip(clip: VideoClip, indexPath: NSIndexPath){

        LLSimpleCamera.requestCameraPermission({
            (granted :Bool) in
            if granted {
                //self.takePhoto(clip)

                let cameraPresenterFrame = self.endFrameForCameraContainer()
                let cameraFrame = self.endFrameForCamera()
                let startFrame = self.startFrameForCellAtIndexPath(indexPath)
                
                let presenterViewController = UIViewController()
                presenterViewController.view.backgroundColor = UIColor.darkGrayColor()
                presenterViewController.view.layer.masksToBounds = true

                let cameraViewController = UIViewController()
                cameraViewController.view.frame = cameraFrame
                presenterViewController.addChildViewController(cameraViewController)
                presenterViewController.view.addSubview(cameraViewController.view)

                self.camera.attachToViewController(cameraViewController, withFrame:cameraFrame)
                self.camera.fixOrientationAfterCapture = true
                self.camera.view.clipsToBounds = true
                
                self.camera.start()
                
                self.camera.onDeviceChange = {
                    (camera :LLSimpleCamera!, device :AVCaptureDevice!) in
                    print("Device changed.")
                }
                
                self.camera.onError = {
                    (camera :LLSimpleCamera!, error: NSError!) in
                    print("Camera error: \(error)")
                }
                
                self.addChildViewController(presenterViewController)
                self.view.addSubview(presenterViewController.view)

                self.cameraShadowView = UIView(frame: startFrame)
                self.cameraShadowView?.backgroundColor = UIColor.darkGrayColor()
                self.cameraShadowView?.addShadow()
                self.view.insertSubview(self.cameraShadowView!, belowSubview: presenterViewController.view)
                
                self.cameraPresenterViewController = presenterViewController

                UIView.animateWithDuration(0.32, delay:0.0, usingSpringWithDamping: 0.6, initialSpringVelocity:0.3, options: .BeginFromCurrentState, animations: {
                    self.cameraPresenterViewController?.view.frame = cameraPresenterFrame
                    self.cameraShadowView?.frame = cameraPresenterFrame
                    
                    }, completion: {
                        (finished :Bool) in

                        let file = VideoFile.instanceWithClip(clip)
                        let duration = self.video.clipDuration()
                        
                        if let url = NSURL(string :file.url!) {

                            self.camera.startRecordingWithOutputUrl(url)
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(Double(duration) * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
                                self.stopRecordingVideo(clip,indexPath: indexPath)
                            })
                            
                        }

                })
                
            }
        })
    }
    
    func stopRecordingVideo(clip :VideoClip, indexPath: NSIndexPath){
        camera!.stopRecording({
            (camera :LLSimpleCamera!, outputFileUrl :NSURL!, error :NSError!) in
            
            print("\(outputFileUrl)")
            if error != nil {
                print("\(error)")
            }
            
            let file = VideoFile.instanceWithClip(clip)
            
            file.url = outputFileUrl.absoluteString
            
            if let image = file.thumbnailFromURL(outputFileUrl) {
                file.setImage(image)
            }
            
            UIView.animateWithDuration(0.16, delay:0.0, usingSpringWithDamping: 0.6, initialSpringVelocity:0.3, options: .BeginFromCurrentState, animations: {
                self.cameraPresenterViewController?.view.frame = self.startFrameForCellAtIndexPath(indexPath)
                self.cameraPresenterViewController?.view.alpha = 0.0
                self.cameraShadowView?.frame = self.startFrameForCellAtIndexPath(indexPath)
                self.cameraShadowView?.alpha = 0.0
                
                }, completion: {
                    (finished :Bool) in
            
                    self.cameraPresenterViewController?.removeFromParentViewController()
                    self.cameraPresenterViewController?.view.removeFromSuperview()
                    self.cameraShadowView?.removeFromSuperview()
                    self.cameraPresenterViewController = nil
                    self.camera?.stop()
            })
            
            do {
                try file.managedObjectContext?.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            self.reloadData()
            
        })
    }
    
    //record clip
    func playClip(clip :VideoClip, indexPath: NSIndexPath){
        playerViewController = bpmVideoMaker.playerViewControllerForClip(clip)
        
        if let _playerViewController :AVPlayerViewController = playerViewController {
            
            let cell = collectionView!.cellForItemAtIndexPath(indexPath) as! BPMCollectionViewCell
            
            selectedIndexPath = indexPath
            
            _playerViewController.view.contentMode = .ScaleAspectFill
            _playerViewController.view.frame = videoFrame()
            
            let tap = UITapGestureRecognizer(target: self, action: Selector("dismissClip"))
            tap.delegate = self
            _playerViewController.view.addGestureRecognizer(tap)
            _playerViewController.view.userInteractionEnabled = true
            
            _playerViewController.view.frame = videoFrame()
            
            view.addSubview(_playerViewController.view)
            player = _playerViewController.player
            
            animatePlayerIn(cell, completion: {
                (finished :Bool) in
                
                self.player?.play()
                NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("playerItemDidReachEnd:"), name: AVPlayerItemDidPlayToEndTimeNotification, object: self.player?.currentItem)
                self.player?.addObserver(self, forKeyPath: "status", options:.New, context: nil)
                
            })
            
        }
    }
    
    func dismissClip(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: self.player?.currentItem)
        self.player?.removeObserver(self, forKeyPath: "status")
        
        if let cell :BPMCollectionViewCell = self.collectionView?.cellForItemAtIndexPath(selectedIndexPath!) as! BPMCollectionViewCell? {
            animatePlayerOut(cell, completion: {
                (finished :Bool) in
                self.selectedIndexPath = nil
                self.playerViewController?.view.removeFromSuperview()
                self.playerViewController = nil
                self.player = nil
            })
        }
    }
    
    func takePhoto(clip :VideoClip){

        cameraPresenterViewController = UIViewController()
        cameraPresenterViewController!.view.backgroundColor = UIColor.whiteColor()
        let frame = self.view.bounds
        camera.view.alpha = 0.0
        camera.start()
        camera!.attachToViewController(cameraPresenterViewController, withFrame: frame)
        presentViewController(cameraPresenterViewController!, animated: true, completion: {
            
            UIView.animateWithDuration(0.32, animations: {
                self.camera.view.alpha = 1.0
                
                }, completion: {
                    (finished :Bool) in

                    self.camera?.capture({
                        (camera :LLSimpleCamera!, image :UIImage!, metadata :[NSObject : AnyObject]!, error :NSError!) in
                        
                        print("error                             :\(error)")
                        print("camera.cameraQuality              :\(camera.cameraQuality)")
                        print("camera.flash                      :\(camera.flash)")
                        print("camera.position                   :\(camera.position)")
                        print("camera.videoEnabled               :\(camera.videoEnabled)")
                        print("camera.recording                  :\(camera.recording)")
                        print("camera.fixOrientationAfterCapture :\(camera.fixOrientationAfterCapture)")
                        print("camera.tapToFocus                 :\(camera.tapToFocus)")
                        print("camera.useDeviceOrientation       :\(camera.useDeviceOrientation)")
                        
                        print("image=\(image.size)")
                        print("metadata=\(metadata)")
                        
                        clip.file?.imageData = UIImageJPEGRepresentation(image, 1.0)
                        
                        self.dismissViewControllerAnimated(true, completion: {
                            self.camera?.stop()
                            self.cameraPresenterViewController = nil
                        })
                        self.reloadData()
                        
                    })

            })
            
            
        })

    }
    
    //gestures: double tap
    public func handleDoubleTap(doubleTap: UITapGestureRecognizer!) {
//        if let cell :BPMCollectionViewCell = doubleTap.view as! BPMCollectionViewCell? {
//            if let indexPath = collectionView?.indexPathForCell(cell) {
//                if let clip = video.clipAtIndexPath(indexPath) as VideoClip? {
//                    //display edit screen
//                }
//            }
//        }
    }
    
    func startFrameForCell(cell :BPMCollectionViewCell) -> CGRect {
        var frame = CGRectZero
        frame.origin = view.center
        if let _frame = collectionView?.convertRect(cell.frame, toView: view) {
            frame = _frame
        }
        return frame
    }

    func startFrameForCellAtIndexPath(indexPath :NSIndexPath) -> CGRect {
        var frame = CGRectZero
        frame.origin = view.center
        if let cellForClip = self.collectionView?.cellForItemAtIndexPath(indexPath) {
            //transform frame if available
            if let _frame = collectionView?.convertRect(cellForClip.frame, toView: view) {
                frame = _frame
            }
        }
        return frame
    }
    
    func videoFrame() -> CGRect {
        let minWidth = min(CGRectGetWidth(view.frame),CGRectGetHeight(view.frame))
        let center = view.center
        let frame = CGRectInset(CGRectMake(center.x - minWidth/2.0, center.y - minWidth/2.0, minWidth, minWidth), 20, 20)
        return frame
    }

    func playerItemDidReachEnd(notification :NSNotification){
//        aPlayer.seekToTime(CMTimeMakeWithSeconds(0, 1))
//        aPlayer.play()
        dismissClip()
    }

    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
//        if object?.isEqual(player) && keyPath == "status" {
//            if player.status == .Failed {
//                print("AVPlayer Failed")
//                
//            } else if player.status == .ReadyToPlay {
//                print("AVPlayerStatusReadyToPlay")
//                player.play()
//                
//                
//            } else if player.status == .Unknown {
//                print("AVPlayer Unknown")
//                
//            }
//        }
    }

    func animatePlayerIn(cell :BPMCollectionViewCell, completion: (finished: Bool) -> ()) {

        let alpha = cell.imageView.alpha

        cell.imageView.alpha = 1.0
        
        let snapshotView = cell.snapshotViewAfterScreenUpdates(true)
        //snapshotView.frame = cell.frame
        snapshotView.frame = startFrameForCell(cell)
        view.addSubview(snapshotView)

        snapshotView.addShadow()
        
        cell.imageView.alpha = alpha
        
        playerViewController?.view.alpha = 0.0
        snapshotView.alpha = 1.0
        
        UIView.animateWithDuration(self.durationAnimateFrame, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .BeginFromCurrentState, animations: {
            snapshotView.frame = self.videoFrame()
            
            }, completion: {
                (finished: Bool) in
                self.playerViewController?.view.alpha = 1.0

                UIView.animateWithDuration(self.durationAnimateAlpha, delay: 0.0, options: .BeginFromCurrentState, animations: {
                    snapshotView.alpha = 0.0
                    
                    }, completion: {
                        (finished :Bool) in
                        completion(finished: finished)
                })
        })
        
    }

    func animatePlayerOut(cell :BPMCollectionViewCell, completion: (finished: Bool) -> ()) {

        if let snapshotView = playerViewController?.view.snapshotViewAfterScreenUpdates(true) {
            //snapshotView.frame = cell.frame
            if let frame = playerViewController?.view.frame {
                snapshotView.frame = frame
            }
            view.addSubview(snapshotView)

            snapshotView.addShadow()
            
            self.playerViewController?.view.alpha = 0.0

            UIView.animateWithDuration(self.durationAnimateFrame/2.0, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .BeginFromCurrentState, animations: {
                snapshotView.frame = self.startFrameForCell(cell)
                
                }, completion: {
                    (finished: Bool) in
                    
                    UIView.animateWithDuration(self.durationAnimateAlpha, delay: 0.0, options: .BeginFromCurrentState, animations: {
                        snapshotView.alpha = 0.0
                        
                        }, completion: {
                            (finished :Bool) in
                            completion(finished: finished)
                    })
            })
        }
        
    }

    public func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
