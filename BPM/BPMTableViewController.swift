//
//  BPMTableViewController.swift
//  BPM
//
//  Created by Liam Dunne on 08/12/2014.
//  Copyright (c) 2014 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import BPMVideo

public class BPMVideoTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
}


class BPMTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var context: NSManagedObjectContext!

    lazy var fetchedResultsController: NSFetchedResultsController = {
        let fetchRequest = NSFetchRequest(entityName: "Video")
        let primarySortDescriptor = NSSortDescriptor(key: "dateUpdated", ascending: false)
        let secondarySortDescriptor = NSSortDescriptor(key: "dateCreated", ascending: false)
        fetchRequest.sortDescriptors = [primarySortDescriptor, secondarySortDescriptor]
        
        let frc = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: self.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        context = CoreDataManager.sharedInstance.managedObjectContext
        reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        CoreDataManager.sharedInstance.logData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    func reloadData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("An error occurred")
        }
        tableView.reloadData()

    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].objects?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BPMVideoTableViewCell", forIndexPath: indexPath) as! BPMVideoTableViewCell
        
        // Configure the cell...
        let video :Video = fetchedResultsController.objectAtIndexPath(indexPath) as! Video

        cell.thumbnailImageView?.image = UIImage(named: "item")
        cell.thumbnailImageView.addSmallShadow()
        
        if let titleText = video.title {
            cell.titleLabel?.text = "\(titleText)"
        }
        if let bpmText = video.bpm?.stringValue {
            cell.bpmLabel?.text = "\(bpmText) BPM"
        }
        if let countText = video.count?.stringValue {
            var plural = "s"
            if video.count == 1 {
                plural = ""
            }
            cell.countLabel?.text = "\(countText) clip\(plural)"
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let sections = fetchedResultsController.sections {
            let currentSection = sections[section]
            return currentSection.name
        }
        
        return nil
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("BPMCollectionViewController", sender: indexPath)
        // do segue
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source

            let video :Video = fetchedResultsController.objectAtIndexPath(indexPath) as! Video
            //delete video
            context.deleteObject(video)
            do {
                try context.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }

            //reload results
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print("An error occurred")
            }
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
//        } else if editingStyle == .Insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        if let bpmCollectionViewController :BPMCollectionViewController = segue.destinationViewController as? BPMCollectionViewController {
            if let indexPath = sender as? NSIndexPath {
                if let video :Video = fetchedResultsController.objectAtIndexPath(indexPath) as? Video {
                    bpmCollectionViewController.video = video
                }
            }
        }
        
        // Pass the selected object to the new view controller.
    }
    
    func didTapAddButton(sender: AnyObject) {
        getNewVideoProperties()
    }
    
    func getNewVideoProperties(){
        //print("# to implement: get new video properties")
        
        let videoCount = Video.allObjects()!.count
        
        let alertController = UIAlertController(title: "New Video", message: "Enter the video details", preferredStyle: .Alert)
        
        let newVideoAction = UIAlertAction(title: "Title", style: .Default) { (_) in
            let titleTextField = alertController.textFields![0] as UITextField
            let countTextField = alertController.textFields![1] as UITextField
            let bpmTextField = alertController.textFields![2] as UITextField
            let beatsPerBarTextField = alertController.textFields![3] as UITextField

            let title = titleTextField.text!
            let count :Int = Int(countTextField.text!)!
            let bpm :Int = Int(bpmTextField.text!)!
            let beatsPerBar :Int = Int(beatsPerBarTextField.text!)!
            
            self.createNewVideo(title, count :count, bpm :bpm, beatsPerBar :beatsPerBar)
            
        }
        newVideoAction.enabled = false
        
        let validateInputs = {
            let titleTextField = alertController.textFields![0] as UITextField
            let countTextField = alertController.textFields![1] as UITextField
            let bpmTextField = alertController.textFields![2] as UITextField
            let beatsPerBarTextField = alertController.textFields![3] as UITextField
            
            let title = titleTextField.text
            let count = Int(countTextField.text!)
            let bpm = Int(bpmTextField.text!)
            let beatsPerBar = Int(beatsPerBarTextField.text!)
            
            let validTitle = (title != "")
            let validCount = (count >= 4 && count <= 128)
            let validBPM = (bpm >= 64 && count <= 180)
            let validBeatsPerBar = (beatsPerBar >= 2 && beatsPerBar <= 8)
            
            let validInputs = validTitle && validCount && validBPM && validBeatsPerBar
            
            newVideoAction.enabled = validInputs
        }
        
        //let forgotPasswordAction = UIAlertAction(title: "Forgot Password", style: .Destructive) { (_) in }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Title"
            textField.text = "New Video \(videoCount)"
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object:textField, queue:NSOperationQueue.mainQueue()) { (notification) in
                validateInputs()
            }
        }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Count"
            textField.text = "8"
            textField.keyboardType = .NumberPad
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object:textField, queue:NSOperationQueue.mainQueue()) { (notification) in
                validateInputs()
            }
        }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "BPM"
            textField.text = "120"
            textField.keyboardType = .NumberPad
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object:textField, queue:NSOperationQueue.mainQueue()) { (notification) in
                validateInputs()
            }
        }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Beats per bar"
            textField.keyboardType = .NumberPad
            textField.text = "4"
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object:textField, queue:NSOperationQueue.mainQueue()) { (notification) in
                validateInputs()
            }
        }
        
        alertController.addAction(newVideoAction)
        //alertController.addAction(forgotPasswordAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: {
//            let titleTextField = alertController.textFields![0] as UITextField
//            titleTextField.becomeFirstResponder()
//            titleTextField.selectedTextRange = titleTextField.textRangeFromPosition(titleTextField.beginningOfDocument, toPosition: titleTextField.endOfDocument)
            validateInputs()
        })
        
    }
    func createNewVideo(title :String, count :Int, bpm :Int, beatsPerBar :Int){
        print("# to implement: create new video")
        print("Title: \(title), Count: \(count), BPM: \(bpm), BeatsPerBar: \(beatsPerBar)")
        
        let bpmVideo = BPMVideo.init()
        bpmVideo.title = title
        bpmVideo.bpm = Float(bpm)
        bpmVideo.count = count
        bpmVideo.multiple = beatsPerBar
        
        let video = Video.instanceWithBPMVideo(bpmVideo)
        
        do {
            try video.managedObjectContext?.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        reloadData()
        
    }
    func presentBPMCollectionViewController(){
        print("# to implement: present BPMCollectionView controller to edit it")
        
    }
    
    
}
